import $ from 'jquery'
import M from 'materialize-css/dist/js/materialize'

chrome.storage.sync.get(['theme'], (result) => {
    $('#theme>label>input').prop('checked', result.theme)
    $('body').show()

    $(() => {
        $('#theme>label>span.lever').click((e) => {
            chrome.runtime.sendMessage({
                theme: !$('#theme>label>input').prop('checked')
            })
        })
    })
})