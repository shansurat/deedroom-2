import $ from 'jquery'

$(() => {
    $('body').addClass('body')

    $('#btnLogout, #btnCapture1').addClass('navbar__button')

    $('ul.rtsUL').addClass('tabs')

    $('.RadListBox, .driHide').remove()

    $('#cbxLoadImageAll').next().addClass('checkBoxLabel')
    $('#upCountiesPN').next('br').remove()

    $('#selCountiesPN, #cbxCountiesPN').wrapAll('<div />')
    $('#selCountiesBP, #cbxCountiesBP').wrapAll('<div />')
    $('#selCountiesFT, #cbxCountiesFT').wrapAll('<div />')
    $('#lblPNSearchType, #ddlBookTypePN').wrapAll('<div />')
    $('#lblBPSearchType, #ddlBookTypeBP').wrapAll('<div />')
    $('#lblFTSearchType, #ddlBookTypeFT').wrapAll('<div />')

    $('#txtbxEndDate, #btnSearchBP, #cbxFTSSearchType').nextAll('br').remove()

    // injectToIframe()

})

function injectToIframe() {

    let head = $('iframe').contents().find('head')

    head.append($('<link />', {
        rel: 'stylesheet',
        href: 'file://document/style.css',
        type: 'text/css'
    }))

    head.append($('<script />'), {
        src: 'file://document/script.js'
    })
}