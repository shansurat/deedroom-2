const autoprefixer = require('autoprefixer')

module.exports = {
    entry: [
        './src/browser-action/script.js',
        './src/browser-action/style.scss',
        './src/browser-action/popup.html'
    ],
    output: {
        path: __dirname + '/dist/browser-action',
        filename: 'script.js',
    },
    module: {
        rules: [{
                test: /\.scss$/,
                use: [{
                        loader: 'file-loader',
                        options: {
                            name: 'style.css',
                        },
                    },
                    {
                        loader: 'extract-loader'
                    },
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: () => [autoprefixer()],
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            includePaths: ['./node_modules']
                        }
                    }
                ]
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['env']
                },
            },
            {
                test: /\.(html)$/,
                use: ['file-loader?name=popup.[ext]', 'extract-loader', 'html-loader']
            }
        ]
    }
}