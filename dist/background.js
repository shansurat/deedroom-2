chrome.tabs.onActivated.addListener((activeInfo) => {
    chrome.tabs.get(activeInfo.tabId, (tab) => {
        if (isDeedroom(tab.url)) {
            chrome.storage.sync.get(['theme'], (result) => {
                if (result.theme)
                    injectDeedroom()
            })
        } else
            chrome.browserAction.disable(tab.id)
    })
})

chrome.tabs.onUpdated.addListener((tabId, changeInfo, Tab) => {
    if (isDeedroom(changeInfo.url))
        chrome.storage.sync.get(['theme'], (result) => {
            if (result.theme)
                injectDeedroom()
        })
})

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if (request.theme !== null) {
        if (request.theme) {
            injectDeedroom()
        } else {
            chrome.tabs.executeScript({
                code: 'location.reload()'
            })
        }


        chrome.storage.sync.set({
            theme: request.theme
        })
    }
})

function injectDeedroom() {
    chrome.tabs.insertCSS({
        file: 'deedroom/style.css'
    }, () => {
        chrome.tabs.executeScript({
            file: 'deedroom/script.js'
        })
    })
}

function isDeedroom(url) {
    if (!url)
        return

    let matches
    matches = url.match(/^https?\:\/\/([^\/?#]+)(?:[\/?#]|$)/i)

    return matches && matches[1] == 'deedroom.com'
}